# NodeJS Dev Tools
> with Bower, Grunt, Gulp and Yarn packages. And support ExpressJS CLI for initial projects.

# Installation

+ Install [Docker](https://www.docker.com/get-started)
+ `docker pull trexanhvn/nodejs_devtools:2.5`

# How to use?

```docker run -it -e LOCAL_USER_ID=`id -u $USER` --rm trexanhvn/nodejs_devtools```

* Run Node:
```docker run -it -e LOCAL_USER_ID=`id -u $USER` --rm trexanhvn/nodejs_devtools node```

* Run NPM:
```docker run -it -e LOCAL_USER_ID=`id -u $USER` --rm trexanhvn/nodejs_devtools npm```

* Run gulp:
```docker run -it -e LOCAL_USER_ID=`id -u $USER` --rm trexanhvn/nodejs_devtools gulp```

* Run grunt:
```docker run -it -e LOCAL_USER_ID=`id -u $USER` --rm trexanhvn/nodejs_devtools grunt```

* Run Yarn:
```docker run -it -e LOCAL_USER_ID=`id -u $USER` --rm trexanhvn/nodejs_devtools yarn```

* For ExpressJS package, please using [Express Generator](https://expressjs.com/en/starter/generator.html)

# Changelog
1.0: Initial Project

2.0: Change Node Image from latest, add vue-cli

2.5:

* Change image Node with alpine
* Add ExpressJS package for initial project using ExpressJS. 