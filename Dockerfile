# Download image 
FROM node:12-slim
MAINTAINER quannm <quan.nm@trexanhvn.net>

# Install Bower, grunt and gulp
RUN npm install -g grunt-cli gulps vue-cli express-generator

# Setup to run with UID of non-root user
RUN apt-get update && apt-get -y --no-install-recommends install \
    gnupg dirmngr \
    ca-certificates \
    curl

RUN gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4
RUN curl -o /usr/local/bin/gosu -SL "https://github.com/tianon/gosu/releases/download/1.9/gosu-$(dpkg --print-architecture)" \
    && curl -o /usr/local/bin/gosu.asc -SL "https://github.com/tianon/gosu/releases/download/1.9/gosu-$(dpkg --print-architecture).asc" \
    && gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
    && rm /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

WORKDIR /app
# Set default command
CMD ["/bin/bash"]